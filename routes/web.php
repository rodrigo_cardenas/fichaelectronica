<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('getPaciente', 'HomeController@getPaciente');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detalleUrgencia/{rut}/{ficha}/{UUID_U}', 'HomeController@detalleUrgencia')->name('detalleUrgencia');
Route::get('/detalleAmbulatorio/{rut}/{ficha}/{UUID_U}', 'HomeController@detalleAmbulatorio')->name('detalleAmbulatorio');
Route::get('/loginPortal', 'Auth\LoginController@loginPortal')->name('loginPortal');
