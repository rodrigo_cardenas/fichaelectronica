<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPortal extends Model
{
    protected $connection = 'mysql9';
    protected $table = 'users';
}
