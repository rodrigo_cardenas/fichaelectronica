<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\WSPaciente;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('detalleAmbulatorio', 'detalleUrgencia');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Obtiene paciente desde WS.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPaciente(Request $request, WSPaciente $client)
    {
        return $client->getPaciente($request);
    }

    /**
     * Obtiene detalle de urgencia desde WS.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detalleUrgencia($rut, $ficha, $UUID_U, WSPaciente $client)
    {
        $data = $client->getUrgencia($rut, $ficha, $UUID_U);
        return view('vUrgencia', compact('data'));
    }

    /**
     * Obtiene detalle de ambulatorio desde WS.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detalleAmbulatorio($rut, $ficha, $UUID_U, WSPaciente $client)
    {
        $data = $client->getAmbulatorio($rut, $ficha, $UUID_U);
        return view('vAmbulatorio', compact('data'));
    }
}
