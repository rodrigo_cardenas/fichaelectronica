<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;

class WSPaciente
{
    protected $baseUriMaestro = 'http://10.4.237.33:3001/api/v1';
    protected $baseUriHospitalizado = 'http://10.4.237.28/api/v1/paciente/ingresos/';
    protected $baseUriAtenciones = 'http://10.4.237.33/salidaws/public/api/fichapaciente';
    protected $baseUriDetalleAmbulatorio = 'http://10.4.237.33/salidaws/public/api/ambulatorio';
    protected $baseUriDetalleUrgencia = 'http://10.4.237.33/salidaws/public/api/urgencia';

    public function getPaciente(Request $request)
    {
        $url = (!empty($request->rut)) ? "{$this->baseUriMaestro}/MPI2/{$request->rut}" : "{$this->baseUriMaestro}/MPIF/{$request->ficha}";
        $urlAtenciones = (!empty($request->rut)) ? "{$this->baseUriAtenciones}/{$request->rut}" : "{$this->baseUriAtenciones}f/{$request->ficha}";
        $json = json_decode(file_get_contents($url), true);

        if (!isset($json['data'][0])) {
            $json['error'] = true;
            $json['message'] = 'No se encuentra el paciente';
        } else {
            $json['data'][0]['edad'] = $this->getEdad($json['data'][0]['fn_paciente']);
            $json['data'][0]['fn_paciente'] = Carbon::createFromFormat('Y-m-d\TH:i:s+', $json['data'][0]['fn_paciente'])->format('d-m-Y');
            $urlHosp = $this->baseUriHospitalizado.$json['data'][0]['no_ficha'];
            $atenciones = "{$urlAtenciones}/{$request->inicio}/{$request->fin}";
            $json['data'][0]['detalleAtenciones'] = json_decode(file_get_contents($atenciones), true);
            $json['data'][0]['detalleHospitalizacion'] = json_decode(file_get_contents($urlHosp), true);
            $json['data'][0]['detallePaciente'] = view('partials.detallePaciente', compact('json'))->render();
            $json['data'][0]['atenciones'] = view('partials.timelinePaciente', compact('json'))->render();
        }
        return $json;
    }

    public function getUrgencia($rut, $ficha, $UUID_U)
    {
        // $url = "{$this->baseUriMaestro}/MPIF/{$ficha}";
        $url = (!empty($rut)) ? "{$this->baseUriMaestro}/MPI2/{$rut}" : "{$this->baseUriMaestro}/MPIF/{$ficha}";
        $json = json_decode(file_get_contents($url), true);
        if (!isset($json['data'][0])) {
            abort(403, 'No se encuentra el paciente.');
        } else {
            $json['data'][0]['edad'] = $this->getEdad($json['data'][0]['fn_paciente']);
            $json['data'][0]['fn_paciente'] = Carbon::createFromFormat('Y-m-d\TH:i:s+', $json['data'][0]['fn_paciente'])->format('d-m-Y');
            $detalleUrgencia = "{$this->baseUriDetalleUrgencia}/{$UUID_U}/";
            $json['detalleUrgencia'] = json_decode(file_get_contents($detalleUrgencia), true);
        }
        return $json;
    }

    public function getAmbulatorio($rut, $ficha, $UUID_U)
    {
        // $url = "{$this->baseUriMaestro}/MPIF/{$ficha}";
        $url = (!empty($rut)) ? "{$this->baseUriMaestro}/MPI2/{$rut}" : "{$this->baseUriMaestro}/MPIF/{$ficha}";
        $json = json_decode(file_get_contents($url), true);
        if (!isset($json['data'][0])) {
            abort(403, 'No se encuentra el paciente.');
        } else {
            $json['data'][0]['edad'] = $this->getEdad($json['data'][0]['fn_paciente']);
            $json['data'][0]['fn_paciente'] = Carbon::createFromFormat('Y-m-d\TH:i:s+', $json['data'][0]['fn_paciente'])->format('d-m-Y');
            $detalleAmbulatorio = "{$this->baseUriDetalleAmbulatorio}/{$UUID_U}/";
            $json['detalleAmbulatorio'] = json_decode(file_get_contents($detalleAmbulatorio), true);
        }
        return $json;
    }

    public function getEdad($fc_nacimiento)
    {
        if ($fc_nacimiento != null) {
            $f1 = Carbon::createFromFormat('Y-m-d\TH:i:s+', $fc_nacimiento);
            $f2 = Carbon::now();
            $anos = $f2->diff($f1)->format('%y años');
            // solo si es menor de 200 Xd años mostrar años con meses y días
            if ($f2->diff($f1)->y < 200) {
                $dias = $f2->diff($f1)->format('%d días');
                $meses = $f2->diff($f1)->format('%m meses');
                $anos = $f2->diff($f1)->format('%y años');

                if ($f2->diff($f1)->format('%d') == '1') {
                    $dias = $f2->diff($f1)->format('%d día');
                }
                if ($f2->diff($f1)->format('%m') == '1') {
                    $meses = $f2->diff($f1)->format('%m mes');
                }
                if ($f2->diff($f1)->format('%y') == '1') {
                    $anos = $f2->diff($f1)->format('%y año');
                }

                if ($f2->diff($f1)->y == 0) {
                    if ($f2->diff($f1)->format('%m') == '0') {
                        return $dias;
                    }
                    return "{$meses} y {$dias}";
                }

                return "{$anos} con {$meses} y {$dias}";
            }
            return $anos;
        }
        return false;
    }
}
