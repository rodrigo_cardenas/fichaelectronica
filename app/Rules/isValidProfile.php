<?php

namespace App\Rules;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class isValidProfile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::whereRut($value)->first();
        return $user != null && in_array($user->perfil_id, [1, 2, 4, 9, 10, 11]);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Debe tener perfil medico para ingresar.';
    }
}
