@php
    $icon = 'fa-bed bg-blue';
    $fechaAdmision = ucwords(\Carbon\Carbon::createFromFormat('Y.m.d', $hospitalizacion[0]['Fecha_Admisión'])->locale('es')->isoFormat('D MMM YYYY'));
    $horaAdmision = \Carbon\Carbon::parse($hospitalizacion[0]['Hora_Admision'])->format('H:i');
    $fechaEgreso = ($hospitalizacion[0]['Fecha_egreso_administrativo']) ? ucwords(\Carbon\Carbon::createFromFormat('Y.m.d', $hospitalizacion[0]['Fecha_egreso_administrativo'])->locale('es')->isoFormat('D MMM YYYY')) : '<b>N/A | Paciente con hospitalización Activa</b>';
    $horaEgreso = ($hospitalizacion[0]['Fecha_egreso_administrativo']) ? \Carbon\Carbon::parse($hospitalizacion[0]['hora_egreso_administrativo'])->format('H:i') : '';
@endphp

<!-- timeline time label -->
<div class="time-label">
    <span class="bg-green">{{ $fechaAdmision }}</span>
</div>

<!-- timeline item -->
<div>
    <i class="fas {{ $icon }}"></i>
    <div class="timeline-item">
        <span class="time">{!! "{$fechaAdmision} <i class='fas fa-clock'></i> {$horaAdmision}" !!}</span>
        <h3 class="timeline-header"><a href="#">{{ $hospitalizacion[0]['TIPO'] }}</a> <small><b>{{ $hospitalizacion[0]['LOCAL'] }}</b></small></h3>

        <div class="timeline-body">
            <small>
                <ul>
                    <li>Fecha de Atención: {{ $fechaAdmision }} {{ $horaAdmision }}</li>
                    <li>Episodio TrakCare: {{ $hospitalizacion[0]['NUMERO_EPISODIO'] }}</li>
                    <li>Fecha de Egreso: {!! $fechaEgreso !!} {{ $horaEgreso }}</li>
                </ul>
            </small>
        </div>
        <div class="timeline-footer">
            <a href="http://10.4.237.29/login?rUser={{ Auth::user()->rut }}&tUser={{ Session::get('token_portal') }}&pac={{ $json['data'][0]['detalleHospitalizacion']['id'] }}" target="_blank" class="btn btn-primary btn-sm">Ver en HSJD Hospitalizados</a>
        </div>
    </div>
</div>
