<ul class="nav flex-column">
    {{-- <li class="nav-item">
        <a class="nav-link">
            Estado <span class="float-right badge bg-primary">Hospitalizado en Servicio Medicina</span>
        </a>
    </li> --}}
    <li class="nav-item">
        <a class="nav-link">
            Nombre <span class="float-right nombreP">{{ $json['data'][0]['nombre_paciente'] }} {{ $json['data'][0]['apellidop_paciente'] }} {{ $json['data'][0]['apellidom_paciente'] }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link">
            Rut <span class="float-right rutP">{{ $json['data'][0]['rut_paciente'] }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link">
            N° Ficha <span class="float-right fichaP">{{ $json['data'][0]['no_ficha'] }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link">
            <small>
                Fecha de Nacimiento 
                <small> 
                    <span class="float-right fechaNacP">{{ $json['data'][0]['fn_paciente'] }} ({{ $json['data'][0]['edad'] }})</span>
                </small>
            </small>
        </a>
    </li>
    {{-- @dump($json['data']) --}}
    <li class="nav-item">
        <a class="nav-link">
            <a style="margin-left: 35%; margin-bottom: 10px" href="http://10.4.237.27/login?recetas_portal=1&rUser={{ Auth::user()->rut }}&tUser={{ request()->session()->get('token') }}&especialidad=33&rut_paciente={{ $json['data'][0]['detalleHospitalizacion']['nr_run'] }}&digito_paciente={{ $json['data'][0]['detalleHospitalizacion']['tx_digito_verificador'] }}&cama=5&sala=Sala%20427&rut_medico={{ Auth::user()->rut }}&diagnostico=Neumonia%20por%20COVID&ficha_paciente={{ $json['data'][0]['no_ficha'] }}" target="_blank" class="btn btn-primary btn-xs">Ver Recetas</a>
        </a>
    </li>
</ul>
