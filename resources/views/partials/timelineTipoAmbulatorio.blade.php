@php
    $icon = 'fa-user-clock bg-purple';
    $fechaAdmision = ucwords(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$hospitalizacion[0]['Fecha_Admisión'])->locale('es')->isoFormat('D MMM YYYY'));
    $horaAdmision = \Carbon\Carbon::parse($hospitalizacion[0]['Hora_Admision'])->format('H:i');
@endphp

<!-- timeline time label -->
<div class="time-label">
    <span class="bg-green">{{ $fechaAdmision }}</span>
</div>

<!-- timeline item -->
<div>
    <i class="fas {{ $icon }}"></i>
    <div class="timeline-item">
        <span class="time">{!! "{$fechaAdmision} <i class='fas fa-clock'></i> {$horaAdmision}" !!}</span>
        <h3 class="timeline-header"><a href="#">{{ $hospitalizacion[0]['TIPO'] }}</a> <small><b>{{ $hospitalizacion[0]['LOCAL'] }}</b></small></h3>

        <div class="timeline-body">
            <small>
                <ul>
                    <li>Fecha de Atención: {{ $fechaAdmision }} {{ $horaAdmision }}</li>
                    <li>Estado de Sitación: {{ $hospitalizacion[0]['rbap_estadocitación'] }}</li>
                    @empty(!$hospitalizacion[0]['rbap_motivonoatendido'])
                        <li>Motivo: {{ $hospitalizacion[0]['rbap_motivonoatendido'] }}</li>
                    @endempty
                </ul>
            </small>
        </div>
        <div class="timeline-footer">
            @php
                $ficha = !empty($json['data'][0]['no_ficha']) ? $json['data'][0]['no_ficha'] : 0; 
            @endphp
            <a target="_blank" href="{{ url('detalleAmbulatorio', ['rut' => $json['data'][0]['rut_paciente'], 'ficha' => $ficha, 'UUID_U' => $hospitalizacion[0]['UUID_U']])}}" class="btn btn-primary btn-sm">Ver más</a>
        </div>
    </div>
</div>
    