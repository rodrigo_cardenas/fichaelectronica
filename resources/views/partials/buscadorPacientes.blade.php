<div class="col-md-4">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-user"></i> Buscar Paciente <small><cite> ingrese rut o ficha</cite></small></h3>
        </div>
        <form id="buscarPaciente" action="javascript:;" class="form-horizontal">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="rut" name="rut" pattern="[0-9]{7,8}-[0-9kK]{1}" placeholder="Rut sin puntos">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ficha" name="ficha" placeholder="Ficha">
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div id="alert-div"></div>
                <button type="submit" class="btn btn-info">Buscar</button>
            </div>

            <div class="card-body p-0" style="display:none;" id="detallePaciente"></div>
        </form>
    </div>
</div>

@push('views_script')
<script src="{{ asset('js/jquery.rut.chileno.min.js') }}"></script>
<script>
    var inicio = 0;
    var fin = 5;
    $( "#buscarPaciente" ).submit(function( event ) {
        inicio = 0;
        // alert si no hay datos:
        if ( $( "#rut" ).val() === "" && $( "#ficha" ).val() === "") {
            $( "#alert-div" ).html('<div class="alert alert-danger" role="alert">Debe ingresar al menos ficha o rut</div>').show().fadeOut(5000);
            event.preventDefault();
            return
        }
        // icono cargando:
        $('#historial-icon').removeClass("fa-history").addClass(" fa-spinner fa-spin");
        // obtener data paciente:
        $.get("{{ url('getPaciente') }}?rut="+$("#rut").val()+"&ficha="+$("#ficha").val()+"&inicio="+inicio+"&fin="+fin, function( json ) {
            if (!json.error) {
                inicio = inicio + fin;
                $(".alert-div").html("");
                $("#detallePaciente").css("display", "");
                $('#historial-icon').removeClass("fa-spinner fa-spin").addClass("fa-history");
                $("#detallePaciente").html(json.data[0].detallePaciente);
                $("#card-timeline").html(json.data[0].atenciones);
            }else{
                $("#alert-div").html('<div class="alert alert-danger" role="alert">'+ json.message +'</div>').show().fadeOut( 5000 );
                event.preventDefault();
            }
        }, "json");
        
        return;
    });

    function verMas() {
        // icono cargando:
        $('#historial-icon').removeClass("fa-history").addClass(" fa-spinner fa-spin");
        $.get("{{ url('getPaciente') }}?rut="+$("#rut").val()+"&ficha="+$("#ficha").val()+"&inicio="+inicio+"&fin="+fin, function( json ) {
            if (!json.error) {
                inicio = inicio + fin;
                $(".btn-mas").css("display", "none");
                $('#historial-icon').removeClass("fa-spinner fa-spin").addClass("fa-history");
                $("#card-timeline").append(json.data[0].atenciones);
            }else{
                $("#alert-div").html('<div class="alert alert-danger" role="alert">'+ json.message +'</div>').show().fadeOut( 5000 );
                event.preventDefault();
            }
        }, "json");
    }

    $("#rut").change(function(){
        var value=$.rut.formatear($("#rut").val());
        $("#rut").val(value);
        if(!$.rut.validar(value)){
            $( "#alert-div" ).html('<div class="alert alert-danger" role="alert">Rut Inválido</div>').show().fadeOut(5000);
        }
    });
</script>
@endpush