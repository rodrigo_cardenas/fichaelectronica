<div class="timeline" style="" id="timelinePaciente">
    
    @forelse ($json['data'][0]['detalleAtenciones'] as $hospitalizacion)
        
        @switch($hospitalizacion[0]['TIPO'])
            @case('Hospitalizado')
                @include('partials.timelineTipoHospitalizado')
                @break
            @case('Urgencias')
                @include('partials.timelineTipoUrgencia')
                @break
            @case('Ambulatorio')
                @include('partials.timelineTipoAmbulatorio')
                @break
            @case('Pabellon')
                @include('partials.timelineTipoPabellon')
                @break
        @endswitch

    @empty
        <h3 style="text-align:center">Sin Atenciones</h3>
    @endforelse

    <br>
    <button class="btn btn-info btn-block btn-mas" style="display:{{ empty($json['data'][0]['detalleAtenciones']) ? 'none' : '' }}; position:absolute;"  id="btn-mas" onclick="verMas(5); //$('#card-timeline').scrollTop(0);">Ver Más</button>
</div>