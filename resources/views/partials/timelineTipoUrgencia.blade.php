@php
    $icon = 'fa-ambulance bg-red';
    $fechaAdmision = ucwords(\Carbon\Carbon::createFromFormat('Y.m.d', $hospitalizacion[0]['Fecha_Admisión'])->locale('es')->isoFormat('D MMM YYYY'));
    $horaAdmision = \Carbon\Carbon::parse($hospitalizacion[0]['Hora_Admision'])->format('H:i');
    $fechaEgreso = ($hospitalizacion[0]['Fecha_egreso_administrativo']) ? ucwords(\Carbon\Carbon::createFromFormat('Y.m.d', $hospitalizacion[0]['Fecha_egreso_administrativo'])->locale('es')->isoFormat('D MMM YYYY')) : '';
    $horaEgreso = \Carbon\Carbon::parse($hospitalizacion[0]['hora_egreso_administrativo'])->format('H:i');
@endphp

<!-- timeline time label -->
<div class="time-label">
    <span class="bg-green">{{ $fechaAdmision }}</span>
</div>

<!-- timeline item -->
<div>
    <i class="fas {{ $icon }}"></i>
    <div class="timeline-item">
        <span class="time">{!! "{$fechaAdmision} <i class='fas fa-clock'></i> {$horaAdmision}" !!}</span>
        <h3 class="timeline-header"><a href="#">{{ $hospitalizacion[0]['TIPO'] }}</a> <small><b>{{ $hospitalizacion[0]['LOCAL'] }}</b></small></h3>

        <div class="timeline-body">
            <small>
                <ul>
                    <li>Fecha de Atención: {{ $fechaAdmision }} {{ $horaAdmision }}</li>
                    <li>Episodio TrakCare: {{ $hospitalizacion[0]['NUMERO_EPISODIO'] }}</li>
                    <li>Fecha de Egreso: {{ $fechaEgreso }} {{ $horaEgreso }}</li>
                </ul>
            </small>
        </div>
        <div class="timeline-footer">
            {{-- <a target="_blank" href="{{ url('detalleUrgencia', ['ficha' => $json['data'][0]['no_ficha'], 'UUID_U' => $hospitalizacion[0]['UUID_U']])}}" class="btn btn-primary btn-sm">Ver más</a> --}}
            <a target="_blank" href="{{ url('detalleUrgencia', ['rut' => $json['data'][0]['rut_paciente'], 'ficha' => $json['data'][0]['no_ficha'], 'UUID_U' => $hospitalizacion[0]['UUID_U']])}}" class="btn btn-primary btn-sm">Ver más</a>
        </div>
    </div>
</div>
    