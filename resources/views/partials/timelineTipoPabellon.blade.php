@php
    $icon = 'fa-hospital bg-yellow';
    $fechaAdmision = ucwords(\Carbon\Carbon::createFromFormat('Y-m-d', $hospitalizacion[0]['fc_intervencion'])->locale('es')->isoFormat('D MMM YYYY'));
    $horaAdmision = \Carbon\Carbon::parse($hospitalizacion[0]['fc_registro'])->format('H:i');
@endphp

<!-- timeline time label -->
<div class="time-label">
    <span class="bg-green">{{ $fechaAdmision }}</span>
</div>

<!-- timeline item -->
<div>
    <i class="fas {{ $icon }}"></i>
    <div class="timeline-item">
        <span class="time">{!! "{$fechaAdmision} <i class='fas fa-clock'></i> {$horaAdmision}" !!}</span>
        <h3 class="timeline-header"><a href="#">{{ $hospitalizacion[0]['TIPO'] }}</a> </h3>

        <div class="timeline-body">
            <small>
                <ul>
                    <li>Fecha de Atención: {{ $fechaAdmision }} {{ $horaAdmision }}</li>
                    <li>Intervención: {{ $hospitalizacion[0]['intervencion'] }}</li>
                    <li>Equipo: {{ $hospitalizacion[0]['equipo'] }}</li>
                </ul>
            </small>
        </div>
        <div class="timeline-footer">
            {{-- <a class="btn btn-primary btn-sm">Ver más</a> --}}
        </div>
    </div>
</div>
    