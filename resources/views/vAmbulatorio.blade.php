@extends('adminlte::page')

@section('title', 'Ficha Electrónica | Ambulatorio')

@section('content_header')

@stop

@section('content')
<div class="row">
	
	{{-- Box Urgencia --}}
	<div class="col-md-12">
		<div class="card card-info">
			<div class="card-header">
				<h3 class="card-title"><i class="fas fa-user-clock"></i> Detalle Ambulatorio</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" onclick="print();" title="Imprimir"><i class="fas fa-print" id="btn-right-top"></i></button>
					{{-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times" id="btn-right-top"></i></button> --}}
				</div>
			</div>
			
            <div class="card-body"  id="card-urgencia">
                <h4 class="page-header"><i class="fa fa-user"></i> Datos Paciente</h4>
                <ul class="" style="columns: 3;">
                    <small>
                        <li><strong>Nombre:</strong> {{ $data['data'][0]['nombre_paciente'] }} {{ $data['data'][0]['apellidop_paciente'] }} {{ $data['data'][0]['apellidom_paciente'] }}</li>
                        <li><strong>Rut:</strong> {{ $data['data'][0]['rut_paciente'] }}</li>
                        <li><strong>Ficha:</strong> {{ $data['data'][0]['no_ficha'] }}</li>
                        <li><strong>Sexo:</strong> {{ $data['data'][0]['sexo_paciente'] }}</li>
                        <li><strong>Fecha Nacimiento:</strong> {{ $data['data'][0]['fn_paciente'] }}</li>
                        <li><strong>Edad:</strong> {{ $data['data'][0]['edad'] }}</li>
                        <li><strong>Nacionalidad:</strong> {{ isset($data['data'][0]['nacionalidad_paciente']) ?? $data['data'][0]['nacionalidad_paciente'] }}</li>
                        <li><strong>Prevision:</strong> {{ $data['data'][0]['Prevision'] }} {{ $data['data'][0]['Plan'] }}</li>
                        <li><strong>Dirección:</strong> {{ $data['data'][0]['direccion_paciente'] }}, {{ isset($data['data'][0]['comuna_paciente']) ?? $data['data'][0]['comuna_paciente'] }}</li>
                        <li><strong>Contacto:</strong> {{ isset($data['data'][0]['Telefono_de_Contacto']) ?? $data['data'][0]['Telefono_de_Contacto'] }}</li>
                    </small>
                </ul>
                <hr>
                @foreach ($data['detalleAmbulatorio'] as $ambulatorio)
                    <h5 class="page-header"><i class="fas fa-file"></i> Detalle Atención Ambulatoria</h5>
                    <ul class="" style="columns: 3;">
                        <small>
                            @foreach ($ambulatorio[0] as $key => $item)
                            <li><strong>{{ str_replace('_', ' ', $key) }}</strong>: {{$item}}</li>
                            @endforeach
                        </small>
                    </ul>
                @endforeach
            </div>
		</div>
	</div>
</div>
@stop

@section('js')
<script> 
	$('.brand-image').removeClass('elevation-3'); 
	$('.brand-image').removeClass('img-circle'); 
	$('.brand-text').css('color', '#10879a'); 
</script>
@stop
