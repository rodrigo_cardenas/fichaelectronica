{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
    <div class="error-page">
    	<h2 class="headline text-warning"> 403</h2>
        <div class="error-content">
			<h3><i class="fas fa-exclamation-triangle text-warning"></i> {{ $exception->getMessage() }}</h3>
			<p>
				Lo sentimos, ha ocurrido un error al obtener el registro.
				Podrías intentar <a href="/home">volver al inicio</a> o contactarte con Informática al anexo 286727.
			</p>
			<img src="https://cataas.com/cat/gif" >
        </div>
    </div>
@stop

