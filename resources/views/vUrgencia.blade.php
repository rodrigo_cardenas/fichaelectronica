@extends('adminlte::page')

@section('title', 'Ficha Electrónica | Urgencia')

@section('content_header')

@stop

@section('content')
<div class="row">
	
	{{-- Box Urgencia --}}
	<div class="col-md-12">
		<div class="card card-info">
			<div class="card-header">
				<h3 class="card-title"><i class="fas fa-ambulance"></i> Detalle Urgencia</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" onclick="print();" title="Imprimir"><i class="fas fa-print" id="btn-right-top"></i></button>
					{{-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times" id="btn-right-top"></i></button> --}}
				</div>
			</div>
			
            <div class="card-body"  id="card-urgencia">
                <h4 class="page-header"><i class="fa fa-user"></i> Datos Paciente</h4>
                <ul class="" style="columns: 3;">
                    <small>
                        <li><strong>Nombre:</strong> {{ $data['data'][0]['nombre_paciente'] }} {{ $data['data'][0]['apellidop_paciente'] }} {{ $data['data'][0]['apellidom_paciente'] }}</li>
                        <li><strong>Rut:</strong> {{ $data['data'][0]['rut_paciente'] }}</li>
                        <li><strong>Ficha:</strong> {{ $data['data'][0]['no_ficha'] }}</li>
                        <li><strong>Sexo:</strong> {{ $data['data'][0]['sexo_paciente'] }}</li>
                        <li><strong>Fecha Nacimiento:</strong> {{ $data['data'][0]['fn_paciente'] }}</li>
                        <li><strong>Edad:</strong> {{ $data['data'][0]['edad'] }}</li>
                        <li><strong>Nacionalidad:</strong> {{ isset($data['data'][0]['nacionalidad_paciente']) ?? $data['data'][0]['nacionalidad_paciente'] }}</li>
                        <li><strong>Prevision:</strong> {{ $data['data'][0]['Prevision'] }} {{ $data['data'][0]['Plan'] }}</li>
                        <li><strong>Dirección:</strong> {{ $data['data'][0]['direccion_paciente'] }}, {{ isset($data['data'][0]['comuna_paciente']) ?? $data['data'][0]['comuna_paciente'] }}</li>
                        <li><strong>Contacto:</strong> {{ isset($data['data'][0]['Telefono_de_Contacto']) ?? $data['data'][0]['Telefono_de_Contacto'] }}</li>
                    </small>
                </ul>
                <hr>
                @foreach ($data['detalleUrgencia'] as $urgencia)
                    @foreach ($urgencia as $key => $urg)
                        <h5 class="page-header"><i class="fas fa-file"></i> {{ str_replace('_', ' ', $key) }}</h5>
                        @if ($key == 'DATOS_URGENCIA')
                            <ul class="products-list product-list-in-box" style="columns: 4;">
                                <small>
                                    @foreach ($urg[0] as $key => $item)
                                        <li><strong>{{ str_replace('_', ' ', $key) }}</strong>: {{$item}}</li>
                                    @endforeach
                                </small>
                            </ul>
                            <hr>
                        @endif
                        @if ($key == 'SIGNOS_VITALES')
                            @foreach ($urg as $value)
                                <ul class="products-list product-list-in-box" style="columns: 4;">
                                    <small>
                                        @foreach ($value as $key => $item)
                                            <li><strong>{{ str_replace('_', ' ', $key) }}</strong>: {{$item}}</li>
                                        @endforeach
                                    </small>
                                </ul>
                                <hr>
                            @endforeach
                        @endif
                        @if ($key == 'ATENCION_MEDICA')
                            @foreach ($urg as $value)
                                <ul class="products-list product-list-in-box" style="columns: 4;">
                                    <small>
                                        @foreach ($value as $key => $item)
                                            <li><strong>{{ str_replace('_', ' ', $key) }}</strong>: {{$item}}</li>
                                        @endforeach
                                    </small>
                                </ul>
                                <hr>
                            @endforeach
                        @endif
                        @if ($key == 'FARMACOS')
                            @foreach ($urg as $value)
                                <ul class="products-list product-list-in-box" style="columns: 4;">
                                    <small>
                                        @foreach ($value as $key => $item)
                                            <li><strong>{{ str_replace('_', ' ', $key) }}</strong>: {{$item}}</li>
                                        @endforeach
                                    </small>
                                </ul>
                                <hr>
                            @endforeach
                        @endif
                        @if ($key == 'INDICACIONES')
                            @foreach ($urg as $value)
                                <ul class="products-list product-list-in-box" style="columns: 2;">
                                    <small>
                                        @foreach ($value as $key => $item)
                                            @if ($key == 'URL_EXAMEN')
                                                {{-- <li> <a target="_blank" class="pull-right" href="{{$item}}"><i class="fa fa-file-pdf"></i> Ver PDF</a> </li> --}}
                                            @else
                                                <li><strong>{{ str_replace('_', ' ', $key) }}</strong>: {{$item}}</li>
                                            @endif
                                        @endforeach
                                    </small>
                                </ul>
                                <hr>
                            @endforeach
                        @endif
                    @endforeach
                @endforeach
            </div>
		</div>
	</div>
</div>
@stop

@section('js')
<script> 
	$('.brand-image').removeClass('elevation-3'); 
	$('.brand-image').removeClass('img-circle'); 
	$('.brand-text').css('color', '#10879a'); 
</script>
@stop
