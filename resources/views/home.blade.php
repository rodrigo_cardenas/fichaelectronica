@extends('adminlte::page')

@section('title', 'Historial Paciente | HSJD')

@section('content_header')

@stop

@section('content')
<div class="row">
	{{-- buscador de pacientes --}}
	@include('partials.buscadorPacientes')
	
	{{-- Box Timeline --}}
	<div class="col-md-8">
		<div class="card card-info">
			<div class="card-header">
				<h3 class="card-title"><i class="fa fa-history" id="historial-icon"></i> Historial</h3>
				<div class="card-tools">
					{{-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times" id="btn-right-top"></i></button> --}}
				</div>
			</div>
			
			<div class="card-body" style="height:550px; overflow-y: scroll;" id="card-timeline"></div>
		</div>
	</div>
</div>
@stop

@section('js')
<script> 
	$('.brand-image').removeClass('elevation-3'); 
	$('.brand-image').removeClass('img-circle'); 
	$('.brand-text').css('color', '#10879a'); 
</script>
@stop
