@extends('adminlte::page')

@section('title', 'Historial Paciente | HSJD')

@section('content_header')

@stop
@section('content')
    <!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
      <a href="/fichaelectronica/login" style="color: rgb(16, 135, 154);"><b>Historial Paciente Hospitalario</b> </a>
    </div>
    
    </div>
    <!-- /.lockscreen-item -->
    <div class="help-block text-center">
      Buscador de pacientes y sus respectivas atenciones (Hospitalizaciones clínicas, urgencia, pabellon y ambulatorio)   
    </div>
    <div class="text-center">
      <a href="/fichaelectronica/login">Ingresar</a>
    </div>
    <div class="lockscreen-footer text-center">
      Volver al portal <b><a href="http://10.6.3.46/portalhsjd/portal.php" class="text-black">aquí</a></b>
    </div>
  </div>
  <!-- /.center -->
@stop
@section('js')
<script> 
	$('.brand-image').removeClass('elevation-3'); 
	$('.brand-image').removeClass('img-circle'); 
	$('.brand-text').css('color', '#10879a'); 
</script>
@stop
